import React, { useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Grid, Typography, TextField, Button, CircularProgress, Avatar, Input } from '@mui/material';
import { register } from '../services/ApiCall'; // Import the register service
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Colors } from '../components/Color';

const RegisterForm = () => {
  const fields = [
    { name: 'username', label: 'Username', type: 'text', validation: Yup.string().required('Username is required') },
    { name: 'email', label: 'Email', type: 'email', validation: Yup.string().email('Invalid email address').required('Email is required') },
    { name: 'password', label: 'Password', type: 'password', validation: Yup.string().required('Password is required').min(6, 'Password must be at least 6 characters') },
    { name: 'phoneNumber', label: 'Phone Number', type: 'text', validation: Yup.string() },
    { name: 'profession', label: 'Profession', type: 'text', validation: Yup.string() },
    { name: 'companyName', label: 'Company Name', type: 'text', validation: Yup.string() },
  ];

  const initialValues = fields.reduce((acc, field) => {
    acc[field.name] = '';
    return acc;
  }, {});

  const validationSchema = Yup.object().shape(
    fields.reduce((acc, field) => {
      acc[field.name] = field.validation;
      return acc;
    }, {})
  );

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const convertFileToBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = () => {
        resolve(reader.result.split(',')[1]);
      };

      reader.onerror = (error) => {
        reject(error);
      };

      reader.readAsDataURL(file);
    });
  };

 const handleSubmit = async (values, { setSubmitting }) => {
  try {
    setLoading(true);
    const profilePicString = values.profilePic ? await convertFileToBase64(values.profilePic) : null;
    const updatedValues = { ...values, profilePic: profilePicString };
    const user = await register(updatedValues);
    console.log('Registration successful:', user);
    toast.success('Registration successful!');
  } catch (error) {
    console.error('Registration failed:', error.message);
    setError(error.message);
  } finally {
    setLoading(false);
    setSubmitting(false);
  }
};


  return (
    <Grid container justifyContent="center" alignItems="center" className='container-from'>
      <Grid item xs={12} sm={8} md={6} lg={4} className='form-container'>
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <Typography variant="h4" align="center">
              Register
            </Typography>
          </Grid>
          <Grid item>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
              {({ values, isSubmitting, setFieldValue }) => (
                <Form>
                  <Grid container direction="column" spacing={2}>
                    <Grid item>
                      <Grid container spacing={2} alignItems="center" justifyContent="center">
                        <Grid item xs={12} sm={12} style={{ textAlign: 'center' }}>
                          <Avatar
                            sx={{ width: 100, height: 100, margin: 'auto', marginBottom: 2, borderRadius: '50%' }}
                            alt="Profile Picture"
                            src={values.profilePic ? URL.createObjectURL(values.profilePic) : 'default-profile-pic.jpg'}
                          />
                          <Input
                            type="file"
                            accept="image/*"
                            onChange={(event) => {
                              setFieldValue('profilePic', event.currentTarget.files[0]);
                            }}
                            sx={{ display: 'none' }}
                            id="upload-profile-pic"
                          />
                          <label htmlFor="upload-profile-pic">
                            <Button variant="outlined" color ='primary' component="span" size='medium'>
                              Upload Picture
                            </Button>
                          </label>
                        </Grid>
                        {fields.map((field, index) => (
                          <Grid item xs={12} sm={6} key={index}>
                            <Field
                              as={TextField}
                              type={field.type}
                              id={field.name}
                              name={field.name}
                              label={field.label}
                              variant="outlined"
                              fullWidth
                            />
                            <ErrorMessage name={field.name} component="div"  style={{color:Colors.errorColor}}/>
                          </Grid>
                        ))}
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        disabled={isSubmitting || loading}
                      >
                        {loading ? <CircularProgress size={24} /> : 'Register'}
                      </Button>
                    </Grid>
                    {error && (
                      <Grid item>
                        <Typography variant="body2" color="error" align="center">
                          {error}
                        </Typography>
                      </Grid>
                    )}
                  </Grid>
                </Form>
              )}
            </Formik>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default RegisterForm;
