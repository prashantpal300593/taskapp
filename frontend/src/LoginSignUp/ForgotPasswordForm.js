import React, { useState } from 'react';
import { Grid, Typography, TextField, Button, CircularProgress } from '@mui/material';
import { Link } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { resetPassword } from '../services/ApiCall'; // Import the resetPassword service
import '../style.css';

const ForgotPasswordForm = () => {
  const initialValues = {
    email: '',
  };

  const validationSchema = Yup.object({
    email: Yup.string().email('Invalid email address').required('Email is required'),
  });

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [successMessage, setSuccessMessage] = useState(null);

  const handleSubmit = async (values, { setSubmitting }) => {
    // try {
    //   setLoading(true);
    //   const response = await resetPassword(values.email);
    //         // const response = await resetPassword(values.email);

    //   console.log(response);
    //   setSuccessMessage('Password reset email sent successfully');
    // } catch (error) {
    //   console.error('Password reset failed:', error);
    //   setError(error.message || 'Password reset failed');
    // } finally {
    //   setLoading(false);
    //   setSubmitting(false);
    // }
  };

  return (
    <Grid container justifyContent="center" alignItems="center" className="container-from">
      <Grid item xs={12} sm={8} md={6} lg={4} className="form-container">
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <Typography variant="h4" align="center">
              Forgot Password
            </Typography>
          </Grid>
          <Grid item>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
              {({ isSubmitting }) => (
                <Form>
                  <Grid container direction="column" spacing={2}>
                    <Grid item>
                      <Field
                        as={TextField}
                        type="email"
                        id="email"
                        name="email"
                        label="Email"
                        variant="outlined"
                        fullWidth
                      />
                      <ErrorMessage name="email" component="div" />
                    </Grid>
                    <Grid item>
                      <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        disabled={isSubmitting || loading}
                      >
                        {loading ? <CircularProgress size={24} /> : 'Reset Password'}
                      </Button>
                    </Grid>
                    {error && (
                      <Grid item>
                        <Typography variant="body2" color="error" align="center">
                          {error}
                        </Typography>
                      </Grid>
                    )}
                    {successMessage && (
                      <Grid item>
                        <Typography variant="body2" color="primary" align="center">
                          {successMessage}
                        </Typography>
                      </Grid>
                    )}
                    <Grid item>
                      <Typography variant="body2" align="center">
                        <Link to="/login">Return to Login</Link>
                      </Typography>
                    </Grid>
                  </Grid>
                </Form>
              )}
            </Formik>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ForgotPasswordForm;
