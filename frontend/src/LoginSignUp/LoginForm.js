import React, { useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { Grid, Typography, TextField, Button, CircularProgress, colors } from '@mui/material';
import { login } from '../services/ApiCall'; // Import the login service
import { Link, useNavigate } from 'react-router-dom'; // Import the useNavigate hook
import '../style.css';
import { Colors } from '../components/Color';

const LoginForm = ({ onLoginSuccess }) => {
  const initialValues = {
    identifier: '',
    password: '',
  };

  const validationSchema = Yup.object({
    identifier: Yup.string().required('Username or Email is required'),
    password: Yup.string().required('Password is required'),
  });

  const navigate = useNavigate(); // Initialize the useNavigate hook
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const handleSubmit = async (values, { setSubmitting }) => {
    try {
      setLoading(true); // Set loading state to true
      const response = await login(values); // Call the login function with form values
      console.log(response); // Log the response
      const { token, user } = response; // Destructure token and user from the response
      localStorage.setItem('token', token); // Save token to local storage
      localStorage.setItem('user', JSON.stringify(user)); // Save user to local storage
      console.log('Login successful:', response); // Log success message
      navigate('/'); // Redirect to the home page
      onLoginSuccess(); // Notify parent component about successful login
    } catch (error) {
      console.error('Login failed:', error); // Log error message
      let errorMessage = 'Login failed'; // Default error message
      if (error && error.message) {
        errorMessage = error.message; // Use error message if available
      } else if (error && error.response && error.response.data && error.response.data.message) {
        errorMessage = error.response.data.message; // Use error message from server response if available
      }
      setError(errorMessage); // Set error state
    } finally {
      setLoading(false); // Set loading state to false
      setSubmitting(false); // Set submitting state to false
    }
  };

  return (
    <Grid container justifyContent="center" alignItems="center" className="container-from">
      <Grid item xs={12} sm={8} md={6} lg={4} className="form-container">
        <Grid container direction="column" spacing={2}>
          <Grid item>
            <Typography variant="h4" align="center">
              Login
            </Typography>
          </Grid>
          <Grid item>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
              {({ isSubmitting }) => (
                <Form>
                  <Grid container direction="column" spacing={2}>
                    <Grid item>
                      <Field
                        as={TextField}
                        type="text"
                        id="identifier"
                        name="identifier"
                        label="Username or Email"
                        variant="outlined"
                        fullWidth
                      />
                      <ErrorMessage name="identifier" component="div"  style={{color:Colors.errorColor}}/>
                    </Grid>
                    <Grid item>
                      <Field
                        as={TextField}
                        type="password"
                        id="password"
                        name="password"
                        label="Password"
                        variant="outlined"
                        fullWidth
                      />
                      <ErrorMessage name="password" component="div" style={{color:Colors.errorColor}} />
                    </Grid>
                    <Grid item>
                      <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        disabled={isSubmitting || loading}
                      >
                        {loading ? <CircularProgress size={24} /> : 'Login'}
                      </Button>
                    </Grid>
                    {error && (
                      <Grid item>
                        <Typography variant="body2" color="error" align="center"  style={{color:Colors.errorColor}}>
                          {error}
                        </Typography>
                      </Grid>
                    )}
                    <Grid item>
                      <Typography variant="body2" align="center">
                        <Link to="/forgotPassword" className="link">
                          Forgot Password?
                        </Link>
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography variant="body2" align="center">
                        Don't have an account? <Link to="/signup" className="link">Register</Link>
                      </Typography>
                    </Grid>
                  </Grid>
                </Form>
              )}
            </Formik>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default LoginForm;
