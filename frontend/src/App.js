import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Header from './components/Header';
import TaskList from './components/TaskList';
import TaskForm from './components/TaskForm';
import LoginForm from './LoginSignUp/LoginForm';
import RegisterForm from './LoginSignUp/RegisterForm';
import UserManagement from './admin/UserManagement';
import HomePage from './HomePage/HomePage';
import ProjectForm from './components/ProjectForm';
import ProjectList from './components/ProjectList';
import ProjectDashboard from './components/ProjectDashboard';
import { ThemeProvider } from '@emotion/react';
import theme from './components/Theme';
import Footer from './components/Footer'; // Import Footer component
import ForgotPasswordForm from './LoginSignUp/ForgotPasswordForm';
import { ToastContainer } from 'react-toastify';
function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);

  useEffect(() => {
    // Check authentication status on component mount
    const token = localStorage.getItem('token');
    setIsAuthenticated(!!token); // Update authentication status
  }, []);

  const handleLogout = () => {
    // Remove user data from local storage
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    setIsAuthenticated(false); // Update authentication status
  };

  const handleLoginSuccess = () => {
    setIsAuthenticated(true); // Update authentication status
  };

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <div>
          {/* Pass isAuthenticated and handleLogout function as props to the Header component */}
          <Header isAuthenticated={isAuthenticated} onLogout={handleLogout} />
          <div>
            <Routes>
              <Route path="/" element={<HomePage />} />
              <Route path="/tasks" element={<TaskList />} />
              <Route path="/tasks/add" element={<TaskForm />} />
              {/* Pass handleLoginSuccess function as a prop to the LoginForm component */}
              <Route path="/login" element={<LoginForm onLoginSuccess={handleLoginSuccess} />} />
              <Route path="/signup" element={<RegisterForm />} />
              <Route path="/userManagement" element={<UserManagement />} />
              <Route path="/projects" element={<ProjectList />} />
              <Route path="/projects/new" element={<ProjectForm />} />
              <Route path="/projects/:projectId" element={<ProjectDashboard />} />
              <Route path="/forgotPassword" element={<ForgotPasswordForm />} />
              
              {/* Add more routes as needed */}
            </Routes>
          </div>  
          {/* Add the Footer component */}
            <ToastContainer />
          <Footer />
        </div>
      </Router>
    </ThemeProvider>
  );
}

export default App;
