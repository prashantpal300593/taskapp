const API_BASE_URL = 'https://taskapp-azure.vercel.app';

const handleResponse = async (response) => {
  if (!response.ok) {
    const errorData = await response.json();
    console.error('Error response:', errorData); // Log the error response
    throw new Error(errorData.message || 'Request failed');
  }
  return response.json();
};

const callApi = async (method, endpoint, data = null) => {
  const url = `${API_BASE_URL}/${endpoint}`;
  const options = {
    method,
    headers: {
      'Content-Type': 'application/json',
    },
  };

  if (data) {
    options.body = JSON.stringify(data);
  }

  try {
    const response = await fetch(url, options);
    return handleResponse(response);
  } catch (error) {
    console.error(`Error calling ${method} ${url}:`, error);
    throw error;
  }
};


export const login = async (credentials) => {
  try {
    return await callApi('POST', 'auth/login', credentials);
  } catch (error) {
    throw new Error('Login failed');
  }
};

export const register = async (userData) => {
  try {
    return await callApi('POST', 'auth/signup', userData);
  } catch (error) {
    throw new Error('Registration failed');
  }
};

// Define  task-related functions similarly

export const getAllProjects = async () => {
  try {
    return await callApi('GET', 'project');
  } catch (error) {
    throw new Error('Error fetching projects');
  }
};

export const createProject = async (projectData) => {
  try {
    return await callApi('POST', 'project', projectData);
  } catch (error) {
    throw new Error('Error creating project');
  }
};
export const updateProject = async (projectId, projectData) => {
  try {
    return await callApi('PUT', `project/${projectId}`, projectData);
  } catch (error) {
    throw new Error('Error updating project');
  }
};

// Assuming you have a function to update the project name in your frontend code
// Update the signature of updateProjectName to accept fetchProjects as a parameter
export const updateProjectName = async (projectId, newName, fetchProjects) => {
  try {
    const response = await callApi('PUT', `project/${projectId}`, { name: newName });
    console.log(response); // Log the response for debugging purposes
    // If the request is successful, update the project list by calling fetchProjects
    fetchProjects();
    return response; // Return the response if needed
  } catch (error) {
    // If an error occurs during the request, throw an error with a descriptive message
    throw new Error('Error updating project name: ' + error.message);
  }
};
// Add this function to your ApiCall.js file

export const assignUserToProject = async (projectId, userId) => {
  try {
    // Make an HTTP POST request to assign user to project
    const response = await callApi('POST', `project/${projectId}/assign`, { userId });
    // Log the response for debugging purposes
    console.log(response);
    // Return the response if needed
    return response;
  } catch (error) {
    // If an error occurs during the request, throw an error with a descriptive message
    throw new Error('Error assigning user to project: ' + error.message);
  }
};




export const deleteProject = async (projectId) => {
  try {
    // Make a DELETE request to the server to delete the project with the given projectId
    return await callApi('DELETE', `project/${projectId}`);
  } catch (error) {
    // If an error occurs during the API call, throw an error with a descriptive message
    throw new Error('Error deleting project');
  }
};


export const getProjectById = async (projectId) => {
  try {
    return await callApi('GET', `project/${projectId}`);
  } catch (error) {
    throw new Error('Error fetching project by ID');
  }
};


// Define other project-related functions similarly

export const getAllTasks = async () => {
  try {
    return await callApi('GET', 'tasks');
  } catch (error) {
    throw new Error('Error fetching tasks');
  }
};


export const createTask = async (taskData) => {
  try {
    return await callApi('POST', 'tasks', taskData);
  } catch (error) {
    throw new Error('Error creating task');
  }
};

export const deleteTask = async (id) => {
  try {
    return await callApi('DELETE', `tasks/${id}`);
  } catch (error) {
    throw new Error('Error deleting task');
  }
};

export const updateTask = async (id, updatedTask) => {
  try {
    const response = await callApi('PUT', `tasks/${id}`, updatedTask);
    // Log the response for debugging purposes
    console.log(response);
    // Return the updated task data
    return response;
  } catch (error) {
    // If an error occurs during the request, throw an error with a descriptive message
    throw new Error('Error updating task: ' + error.message);
  }
};
export const updateTaskOnDrop = async (taskId, destinationIndex) => {
  try {
    const response = await callApi('PUT', `tasks/${taskId}/order`, { destinationIndex });
    console.log(response); // Log the response for debugging purposes
    return response; // Return the response if needed
  } catch (error) {
    throw new Error('Error updating task order on drop: ' + error.message);
  }
};




// Define  user-related functions similarly

export const getUsers = async () => {
  try {
    return await callApi('GET', 'users');
  } catch (error) {
    throw new Error('Error fetching users');
  }
};

export const getUserById = async (userId) => {
  try {
    return await callApi('GET', `users/${userId}`);
  } catch (error) {
    throw new Error('Failed to fetch user');
  }
};

export const createUser = async (userData) => {
  try {
    return await callApi('POST', 'users', userData);
  } catch (error) {
    throw new Error('Failed to create user');
  }
};

export const updateUser = async (userId, userData) => {
  try {
    return await callApi('PUT', `users/${userId}`, userData);
  } catch (error) {
    throw new Error('Failed to update user');
  }
};

export const deleteUser = async (userId) => {
  try {
    return await callApi('DELETE', `users/${userId}`);
  } catch (error) {
    throw new Error('Failed to delete user');
  }
};
export const getTasksByUser = async (projectId, userId) => {
  try {
    // Make an HTTP GET request to fetch tasks by user
    const response = await callApi('GET', `tasks/${projectId}/user/${userId}`);
          console.log(response)

    // Check if the response is successful and contains tasks
    if (response) {
      // Return the array of tasks
      return response;
    } else {
      // If the response is not as expected, throw an error
      throw new Error('Error fetching tasks by user');
    }
  } catch (error) {
    // If an error occurs during the request, throw an error with a descriptive message
    throw new Error('Error fetching tasks by user: ' + error.message);
  }
};

