import React from 'react';
import TaskItem from './TaskItem';
import { Paper, Typography, List } from '@mui/material';

const TaskColumn = ({ title, tasks }) => {
  return (
    <Paper style={{ padding: '10px', minHeight: '300px' }}>
      <Typography variant="h6">{title}</Typography>
      <List>
        {tasks.map(task => (
          <TaskItem key={task.id} task={task} />
        ))}
      </List>
    </Paper>
  );
};

export default TaskColumn;
