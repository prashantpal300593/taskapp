import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Typography, CircularProgress, Grid } from '@mui/material';
import TaskItem from './TaskItem';
import { getAllTasks, deleteTask, updateTaskOnDrop } from '../services/ApiCall';
import { getUsers } from '../services/ApiCall';
import { addTask, removeTask, updateTaskOrder } from '../Redux/slice/taskSlice'; // Update import statement
import { addUser } from '../Redux/slice/userSlice'; // Import addUser action

const TaskList = () => {
  const tasks = useSelector(state => state.tasks.tasks);
  const users = useSelector(state => state.users.users);
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const tasksData = await getAllTasks();
        const usersData = await getUsers();
        dispatch(addTask(tasksData));
        dispatch(addUser(usersData));
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };

    fetchData();
  }, [dispatch]);

  const handleDelete = async (id) => {
    try {
      await deleteTask(id);
      dispatch(removeTask(id));
    } catch (error) {
      setError(error.message);
    }
  };

  const handleDragStart = (e, id) => {
    e.dataTransfer.setData('taskId', id);
  };

  const handleDragOver = (e) => {
    e.preventDefault();
  };

const handleDrop = (e) => {
  e.preventDefault();
  const taskId = e.dataTransfer.getData('taskId');
  const destinationIndex = Number(e.target.dataset.index);
  const draggedTaskIndex = tasks.findIndex(task => task._id === taskId);

  // Ensure taskId and index are valid
  if (destinationIndex === -1 || draggedTaskIndex === -1) {
    return;
  }

  // Optimistically update Redux state
  const updatedTasks = [...tasks];
  const [draggedTask] = updatedTasks.splice(draggedTaskIndex, 1);
  updatedTasks.splice(destinationIndex, 0, draggedTask);
  dispatch(updateTaskOrder(updatedTasks));

  // Make API call to update backend
  updateTaskOnDrop(taskId, destinationIndex)
    .then(() => {
      console.log('Task order updated successfully on the backend');
    })
    .catch((error) => {
      console.error('Error updating task order on the backend:', error);
      // Revert changes in Redux state if API call fails
      dispatch(updateTaskOrder(tasks));
    });
};


  return (
    <div style={{ padding: "10px" }}>
      <Typography variant="h4">Tasks</Typography>
      {loading ? (
        <div style={{ textAlign: 'center' }}>
          <CircularProgress />
        </div>
      ) : error ? (
        <Typography variant="body1" color="error">
          Error: {error}
        </Typography>
      ) : (
        <Grid container spacing={2} onDragOver={handleDragOver} onDrop={handleDrop}>
          {tasks.length === 0 ? (
            <Typography variant="body1">No tasks found.</Typography>
          ) : (
            tasks.map((task, index) => (
              <Grid item xs={12} sm={6} md={4} key={task._id} data-index={index} draggable onDragStart={(e) => handleDragStart(e, task._id)}>
                <TaskItem
                  task={task}
                  index={index}
                  users={users}
                  onDelete={handleDelete}
                />
              </Grid>
            ))
          )}
        </Grid>
      )}
    </div>
  );
};

export default TaskList;
