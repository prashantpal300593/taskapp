import React, { useState, useEffect} from 'react';
import { ListItem, ListItemText, ListItemSecondaryAction, IconButton, Dialog, DialogTitle, DialogContent, TextField, Button, Grid, Autocomplete, MenuItem, Select, FormControl, InputLabel, Typography, Paper, Menu } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';

import MoreHorizOutlinedIcon from '@mui/icons-material/MoreHorizOutlined';
import { updateTask, deleteTask } from '../services/ApiCall';
import { getUserById, getUsers, getAllProjects } from '../services/ApiCall';
import { useDispatch } from 'react-redux';
import { setTasks } from '../Redux/slice/taskSlice';
import DeleteIcon from '@mui/icons-material/Delete';
import { Colors } from '../components/Color';
import { Draggable } from 'react-beautiful-dnd';
import 'react-toastify/dist/ReactToastify.css';

const TaskItem = ({ task, index, onDelete, onUpdate }) => {
  const [editMode, setEditMode] = useState(false);
  const [editedTask, setEditedTask] = useState(task);
  const [assignedUserName, setAssignedUserName] = useState('');
  const [users, setUsers] = useState([]);
  const [projects, setProjects] = useState([]);
  const [allStages, setAllStages] = useState(['New', 'In Progress', 'Implemented', 'Done']);
  const [confirmDelete, setConfirmDelete] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null); // State for anchor element of the menu
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchAssignedUserName = async () => {
      try {
        const user = await getUserById(task.assignedUser);
        setAssignedUserName(user.username);
      } catch (error) {
        console.error('Error fetching assigned user name:', error);
      }
    };
    fetchAssignedUserName();
  }, [task]);

  useEffect(() => {
    const fetchAllUsers = async () => {
      try {
        const allUsers = await getUsers();
        setUsers(allUsers);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };
    fetchAllUsers();
  }, []);

  useEffect(() => {
    const fetchProjects = async () => {
      try {
        const projects = await getAllProjects();
        setProjects(projects);
      } catch (error) {
        console.error('Error fetching projects:', error);
      }
    };
    fetchProjects();
  }, []);

  useEffect(() => {
    setEditedTask(task);
  }, [task]);

  const handleEdit = () => {
    setEditMode(true);
  };

  const handleSave = async () => {
    try {
      await updateTask(task._id, {
        title: editedTask.title,
        description: editedTask.description,
        stage: editedTask.stage,
        projectId: editedTask.projectId,
      });

      const updatedTask = { ...task, ...editedTask };

      setEditMode(false);
      setEditedTask(updatedTask);

      onUpdate(updatedTask);
    } catch (error) {
      console.error('Error updating task:', error);
    }
  };

  const handleCancel = () => {
    setEditMode(false);
    setEditedTask(task);
  };

  const handleDelete = async () => {
    if (confirmDelete) {
      try {
        await deleteTask(task._id);
        onDelete(task._id);
        dispatch(setTasks());
      } catch (error) {
        console.error('Error deleting task:', error);
      }
      setConfirmDelete(false);
    } else {
      setConfirmDelete(true);
    }
  };

  const handleTitleChange = (e) => {
    setEditedTask({
      ...editedTask,
      title: e.target.value
    });
  };

  const handleDescriptionChange = (e) => {
    setEditedTask({
      ...editedTask,
      description: e.target.value
    });
  };

  const handleAssignedUserChange = (event, newValue) => {
    setEditedTask({
      ...editedTask,
      assignedUser: newValue ? newValue._id : null
    });
  };

  const handleStageChange = (e) => {
    setEditedTask({
      ...editedTask,
      stage: e.target.value
    });
  };

  const handleProjectChange = (e) => {
    setEditedTask({
      ...editedTask,
      projectId: e.target.value
    });
  };

  const formattedCreationTime = task.createdAt ? new Date(task.createdAt).toLocaleString() : '';

  // Handle click on menu icon
  const handleMenuClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  // Handle close of menu
  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  return (
    <Draggable draggableId={task._id} index={index}>
      {(provided) => (
        <div
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          ref={provided.innerRef}
        >
          <Paper elevation={3} sx={{ position: 'relative' }}>
            <ListItem sx={{ borderBottom: '1px solid #ccc', marginBottom: '10px', boxShadow: Colors.boxShadow }}>
              <ListItemText
                primary={task.title}
                secondary={
                  <>
                    <Typography variant="body2" color="textSecondary">
                      {task.description}
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      <strong>Assigned User:</strong> {assignedUserName}
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                      <strong>Creation Date:</strong> {formattedCreationTime}
                    </Typography>
                  </>
                }
              />
              <ListItemSecondaryAction sx={{top:"15%"}}>
                <IconButton aria-label="more" onClick={handleMenuClick}>
                  <MoreHorizOutlinedIcon />
                </IconButton>
                <Menu
                  anchorEl={anchorEl}
                  open={Boolean(anchorEl)}
                  onClose={handleCloseMenu}
                >
                  <MenuItem onClick={handleEdit}>
                    <EditIcon />
                    Edit
                  </MenuItem>
                  <MenuItem onClick={handleDelete}>
                    <DeleteIcon />
                    Delete
                  </MenuItem>
                </Menu>
              </ListItemSecondaryAction>
            </ListItem>
            <Dialog open={editMode} onClose={handleCancel}>
              <DialogTitle>Edit Task</DialogTitle>
              <DialogContent>
                <TextField
                  label="Title"
                  value={editedTask.title}
                  onChange={handleTitleChange}
                  fullWidth
                  margin="normal"
                />
                <TextField
                  label="Description"
                  value={editedTask.description}
                  onChange={handleDescriptionChange}
                  fullWidth
                  margin="normal"
                />
                <FormControl fullWidth margin="normal">
                  <InputLabel>Stage</InputLabel>
                  <Select
                    value={editedTask.stage}
                    onChange={handleStageChange}
                  >
                    {allStages.map((stage) => (
                      <MenuItem key={stage} value={stage}>{stage}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <FormControl fullWidth margin="normal">
                  <InputLabel>Project Name</InputLabel>
                  <Select
                    value={editedTask.projectId}
                    onChange={handleProjectChange}
                  >
                    {projects.map((project) => (
                      <MenuItem key={project.id} value={project.id}>{project.name}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
                <Autocomplete
                  options={users}
                  getOptionLabel={(option) => option.username}
                  value={users.find(user => user.id === editedTask.assignedUser) || null}
                  onChange={handleAssignedUserChange}
                  renderInput={(params) => <TextField {...params} label="Assigned User" />}
                />
                <div style={{ marginTop: '15px', textAlign: 'right' }}>
                  <Button onClick={handleSave} variant="contained" color="primary" style={{ marginRight: '10px' }}>
                    Save
                  </Button>
                  <Button onClick={handleCancel} variant="outlined" color="error">
                    Cancel
                  </Button>
                </div>
              </DialogContent>
            </Dialog>
            <Dialog
              open={confirmDelete}
              onClose={() => setConfirmDelete(false)}
            >
              <DialogTitle>Confirm Delete</DialogTitle>
              <DialogContent>
                <Typography>Are you sure you want to delete this task?</Typography>
                <div style={{ marginTop: '15px', textAlign: 'right' }}>
                  <Button onClick={handleDelete} variant="contained" color="error" style={{ marginRight: '10px' }}>
                    Yes
                  </Button>
                  <Button onClick={() => setConfirmDelete(false)} variant="outlined" color="primary">
                    No
                  </Button>
                </div>
              </DialogContent>
            </Dialog>
          </Paper>
        </div>
      )}
    </Draggable>
  );
};

export default TaskItem;
