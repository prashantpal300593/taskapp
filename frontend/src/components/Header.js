import React, { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { AppBar, Toolbar, Typography, IconButton, Stack, Drawer, List, ListItem, ListItemIcon, ListItemText, Divider, Menu, MenuItem, useMediaQuery, useTheme, Avatar } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import AddIcon from '@mui/icons-material/Add';
import LogoutIcon from '@mui/icons-material/Logout';
import { getAllProjects } from '../services/ApiCall'; // Import the service to fetch projects
import ProjectList from './ProjectList';
import { Colors } from './Color';
import LogoImage from '../assets/logo.jpg'; // Import the logo image

const Header = ({ onLogout, isAuthenticated }) => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [projects, setProjects] = useState([]);
  const [showProjectMenu, setShowProjectMenu] = useState(false); // State to track the visibility of the project list dropdown
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  const [userInfo, setUserInfo] = useState(null);
  const navigate = useNavigate(); // Replace useHistory with useNavigate

  useEffect(() => {
    const token = localStorage.getItem('token');
    const storedUserInfo = localStorage.getItem('user');
    setUserInfo(storedUserInfo ? JSON.parse(storedUserInfo) : null);
  }, [isAuthenticated]);

  useEffect(() => {
    fetchProjects();
  }, []);

  const fetchProjects = async () => {
    try {
      const projectList = await getAllProjects();
      if (Array.isArray(projectList)) {
        setProjects(projectList);
      } else {
        console.error('getAllProjects did not return an array:', projectList);
      }
    } catch (error) {
      console.error('Error fetching projects:', error);
    }
  };

  const handleLogout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    onLogout();
    navigate('/login'); // Use navigate to redirect to login page after logout
  };

  const toggleDrawer = () => {
    setIsDrawerOpen(!isDrawerOpen);
  };

  const handleMenuClick = (event, menuType) => {
    if (menuType === 'projects') {
      setAnchorEl(event.currentTarget);
      setShowProjectMenu(true); // Set showProjectMenu state to true when the project menu is clicked
    } else if (menuType === 'profile') {
      setAnchorEl(event.currentTarget);
    }
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
    setShowProjectMenu(false); // Set showProjectMenu state to false when the project menu is closed
  };

  const handleProjectSelect = (projectId) => {
    console.log('Selected project:', projectId);
    handleCloseMenu();
  };

  const listItemSx = {
    color: Colors.text,
    textDecoration: 'none',
  };

  const listItemTextSx = {
    flexGrow: 1,
  };

  const renderMenuItems = (data) => {
    return data.map((item, index) => (
      <ListItem
        key={index}
        button
        component={Link}
        to={item.link}
        onClick={item.onClick}
        sx={{ color: Colors.text, textDecoration: 'none', flexGrow: 1 }}
      >
        {item.avatar && (
          <Avatar alt={item.text}  src={`data:image/jpeg;base64, ${item.avatar}`} sx={{ width: 32, height: 32, marginRight: 1 }} />
        )}
        <ListItemText primary={item.text} sx={{ flexGrow: 1 }} />
      </ListItem>
    ));
  };

  const authMenu = (
    <Stack direction="row" spacing={2}>
      {renderMenuItems([
        { text: 'Projects', link: '#', onClick: (event) => handleMenuClick(event, 'projects') },
        { text: 'MyTasks', link: '/tasks' },
        { text: 'UserManagement', link: '/userManagement' },
        { text: userInfo?.username || '',
          avatar: userInfo?.profilePic || '', // Assuming profilePic is the URL of the profile picture
          onClick: (event) => handleMenuClick(event, 'profile')
        },
      ])}
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleCloseMenu}
        onEntered={() => setShowProjectMenu(true)} // Ensure that the project menu is shown after it's opened
      >
        {showProjectMenu && (
          <MenuItem>
            <ProjectList
              projects={projects}
              handleProjectSelect={handleProjectSelect}
              toggleDrawer={toggleDrawer}
            />
          </MenuItem>
        )}
        {!showProjectMenu && (
          <MenuItem onClick={handleLogout}>
            <ListItemIcon>
              <LogoutIcon />
            </ListItemIcon>
            <ListItemText primary="Logout" />
          </MenuItem>
        )}
      </Menu>
    </Stack>
  );

  const guestMenu = (
    <Stack direction="row" spacing={2}>
      <ListItem button component={Link} to="/login" sx={listItemSx}>
        <ListItemText primary="Login" sx={listItemTextSx} />
      </ListItem>
      <ListItem button component={Link} to="/signup" sx={listItemSx}>
        <ListItemText primary="Signup" sx={listItemTextSx} />
      </ListItem>
    </Stack>
  );

  return (
    <AppBar position="static" sx={{ backgroundColor: Colors.background, boxShadow: Colors.boxShadow }}>
      <Toolbar>
       {isAuthenticated  && <IconButton
          edge="start"
          color={Colors.text}
          aria-label="menu"
          onClick={toggleDrawer}
          sx={{ display: 'block', mr: 1 }}
        >
          <MenuIcon />
        </IconButton>}
        <Avatar
          alt="Logo"
          src={LogoImage}
          sx={{ width: 40, height: 40, marginRight: 1 }}
        />
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          <Link to="/" style={{ color: Colors.text, textDecoration: 'none', flexGrow: 1 }}>
            Task Management
          </Link>
        </Typography>
        {isMobile ? null : isAuthenticated ? authMenu : guestMenu}
      </Toolbar>
    {isAuthenticated ?  <Drawer
        anchor="left"
        open={isDrawerOpen}
        onClose={toggleDrawer}
        sx={{ width: 250 }}
      >
        <List>
          <ListItem button component={Link} to="/">
            <Avatar
              alt="Logo"
              src={LogoImage}
              sx={{ width: 40, height: 40, marginRight: 1 }}
            />
            <ListItemText primary="Task Management" sx={{ flexGrow: 1 }} />
          </ListItem>
          <Divider />
          <ListItem button component={Link} to="/projects/new">
            <ListItemIcon>
              <AddIcon />
            </ListItemIcon>
            <ListItemText primary="New Project" sx={listItemTextSx} />
          </ListItem>
          <ProjectList
            handleProjectSelect={handleProjectSelect}
            toggleDrawer={toggleDrawer}
          />
          <ListItem button component={Link} to="/tasks" onClick={toggleDrawer} sx={listItemSx}>
            <ListItemText primary="Tasks" sx={listItemTextSx} />
          </ListItem>
          <ListItem button component={Link} to="/tasks/add">
            <ListItemIcon>
              <AddIcon />
            </ListItemIcon>
            <ListItemText primary="Create Task" sx={{ flexGrow: 1 }} />
          </ListItem>
        </List>
      </Drawer> : ""}
    </AppBar>
  );
};

export default Header;
