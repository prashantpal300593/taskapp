// colors.js
export const Colors = {
  primary: '#1976D2', // Adjust this to your primary color
  secondary: '#FF4081', // Adjust this to your secondary color
  text: '#000', // Adjust this to your text color
  background: '#fff', // Adjust this to your background color
  success: '#0f410f',
  boxShadow:"rgba(0, 30, 43, 0.1) 0px 4px 4px 0px",
  footerBg:"#f1f1f1",
  errorColor:"red"
  // Add more colors as needed
};
