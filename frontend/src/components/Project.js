import React, { useState } from 'react';
import { ListItem, ListItemText, ListItemSecondaryAction, IconButton, Menu, MenuItem, Dialog, DialogTitle, DialogContent, DialogActions, Button, Snackbar } from '@mui/material';
import { MoreHorizOutlined, Edit, Delete } from '@mui/icons-material';
import { Link } from 'react-router-dom';
import { Colors } from './Color';
import { deleteProject, updateProjectName } from '../services/ApiCall'; // Import delete and update API functions

const Project = ({ project, onEdit, onDelete }) => {
  const [menuAnchorEl, setMenuAnchorEl] = useState(null);
  const [deleteConfirmationOpen, setDeleteConfirmationOpen] = useState(false);
  const [deleteSuccessMessage, setDeleteSuccessMessage] = useState('');

  const handleMenuOpen = (event) => {
    setMenuAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setMenuAnchorEl(null);
  };

  const handleEditClick = () => {
    onEdit(project);
    handleMenuClose();
  };

  const handleDeleteClick = () => {
    setMenuAnchorEl(null);
    setDeleteConfirmationOpen(true);
  };

  const handleDeleteConfirmed = async () => {
    try {
      await deleteProject(project._id); // Call delete API with project ID
      onDelete(project); // Remove the deleted project from the UI
      setDeleteSuccessMessage(`Project "${project.name}" deleted successfully.`);
      setDeleteConfirmationOpen(false);
    } catch (error) {
      console.error('Error deleting project:', error);
      // Handle error, display error message, etc.
    }
  };

  const handleDeleteCancelled = () => {
    setDeleteConfirmationOpen(false);
  };

  const handleUpdateProjectName = async (newName) => {
    try {
      await updateProjectName(project._id, newName); // Call update API with project ID and new name
      // Optionally, update the project name in the UI after successful update
    } catch (error) {
      console.error('Error updating project name:', error);
      // Handle error, display error message, etc.
    }
  };

  return (
    <div>
        <ListItem button>
          <Link to={`/projects/${project._id}`} style={{ textDecoration: 'none', color: Colors.text }}>
            <ListItemText primary={project.name} />
          </Link>

          <ListItemSecondaryAction>
            <IconButton onClick={handleMenuOpen}>
              <MoreHorizOutlined />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      <Menu
        anchorEl={menuAnchorEl}
        open={Boolean(menuAnchorEl)}
        onClose={handleMenuClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        getContentAnchorEl={null}
      >
        <MenuItem onClick={handleEditClick}>
          <Edit />
          Edit
        </MenuItem>
        <MenuItem onClick={handleDeleteClick}>
          <Delete />
          Delete
        </MenuItem>
      </Menu>
      <Dialog open={deleteConfirmationOpen} onClose={handleDeleteCancelled}>
        <DialogTitle>Confirm Deletion</DialogTitle>
        <DialogContent>
          Are you sure you want to delete the project "{project.name}"?
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDeleteCancelled} color="primary">
            Cancel
          </Button>
          <Button onClick={handleDeleteConfirmed} color="primary">
            Delete
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar
        open={!!deleteSuccessMessage}
        autoHideDuration={6000}
        onClose={() => setDeleteSuccessMessage('')}
        message={deleteSuccessMessage}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
      />
    </div>
  );
};

export default Project;
