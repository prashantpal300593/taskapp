import React from 'react';
import { Typography, Grid } from '@mui/material';
import { Link } from 'react-router-dom';
import { Colors } from './Color';

const Footer = () => {
  return (
    <Grid container justifyContent="center" alignItems="center" style={{paddingTop:"15px", paddingBottom:"15px", backgroundColor:Colors.footerBg}} className="footer">
      <Grid item>
        <Typography variant="body2" align="center">
          TaskApp | Project Management Software | <Link to="/about">About TaskApp</Link>
        </Typography>
      </Grid>
    </Grid>
  );
};

export default Footer;
