import React, { useState, useEffect } from 'react';
import { TextField, Button, Typography, Grid, FormControl, InputLabel, Select, MenuItem, Chip, Snackbar } from '@mui/material';
import { createProject, getUsers, assignUserToProject } from '../services/ApiCall';

const ProjectForm = ({ onProjectCreated }) => {
  const [name, setName] = useState('');
  const [company, setCompany] = useState('');
  const [users, setUsers] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [saveSuccessMessage, setSaveSuccessMessage] = useState('');

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const usersData = await getUsers();
        console.log('Users Data:', usersData); // Log the fetched user data
        setUsers(prevUsers => [...prevUsers, ...usersData]); // Update state using a callback
        console.log(users)
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };

    fetchUsers();
  }, []);

  const handleRemoveUser = (userId) => {
    setSelectedUsers(prevSelectedUsers => prevSelectedUsers.filter(id => id !== userId));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await createProject({ name, company, assignedUsers: selectedUsers });
      const newProject = response.data;
      onProjectCreated(newProject);
      setName('');
      setCompany('');
      setSelectedUsers([]);
      setSaveSuccessMessage('Project saved successfully.');
    } catch (error) {
      console.error('Error creating project:', error);
    }
  };

  const handleAssignUsers = async (newProject) => {
    try {
      await Promise.all(selectedUsers.map(user => assignUserToProject(newProject._id, user)));
    } catch (error) {
      console.error('Error assigning users to project:', error);
    }
  };

  return (
    <Grid container justifyContent="center" alignItems="center" className='container-from'>
      <Grid item xs={12} sm={8} md={6} lg={4} className='form-container'>
        <div>
          <Typography variant="h4" align="center" gutterBottom>
            Create New Project
          </Typography>
          <form onSubmit={handleSubmit}>
            <TextField
              label="Project Name"
              value={name}
              onChange={(e) => setName(e.target.value)}
              fullWidth
              margin="normal"
              variant="outlined"
              color="primary"
            />
            <TextField
              label="Company Name"
              value={company}
              onChange={(e) => setCompany(e.target.value)}
              fullWidth
              margin="normal"
              variant="outlined"
              color="primary"
            />
            {/* Chips for displaying selected users */}
            <div style={{ marginTop: '10px', marginBottom: '10px' }}>
              {selectedUsers.map(userId => {
                const user = users.find(u => u.id === userId);
                if(user){
                return (
                  <Chip
                    key={userId}
                    label={user.name}
                    onDelete={() => handleRemoveUser(userId)}
                    color="primary"
                    style={{ marginRight: '5px', marginBottom: '5px' }}
                  />
                );}
              })}
            </div>
            {/* Select dropdown for assigning users */}
            <FormControl fullWidth variant="outlined">
              <InputLabel id="select-user-label">Assign Users</InputLabel>
              <Select
                labelId="select-user-label"
                id="select-user"
                multiple
                value={selectedUsers}
                onChange={(e) => setSelectedUsers(e.target.value)}
                label="Assign Users"
                renderValue={(selected) => (
                  <div>
                    {selected.map((userId) => {
                      const user = users.find(u => u._id === userId);
                      return (
                        <Chip
                          key={userId}
                          label={user?.username || 'User not found'}
                          onDelete={() => handleRemoveUser(userId)}
                          color="primary"
                          style={{ marginRight: '5px', marginBottom: '5px' }}
                        />
                      );
                    })}
                  </div>
                )}
              >
                {users.map((user) => (
                  <MenuItem key={user._id} value={user._id}>
                    {user.username}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              style={{ marginTop: '10px', width: '100%' }}
            >
              Create Project
            </Button>
          </form>
          <Snackbar
            open={!!saveSuccessMessage}
            autoHideDuration={6000}
            onClose={() => setSaveSuccessMessage('')}
            message={saveSuccessMessage}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
          />
        </div>
      </Grid>
    </Grid>
  );
};

export default ProjectForm;
