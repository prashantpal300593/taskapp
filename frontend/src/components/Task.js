// Task.js - Component to display individual tasks

import React from 'react';
import { Card, CardContent, Typography, Button } from '@mui/material';

const Task = ({ task, onMoveTask }) => {
  return (
    <Card variant="outlined" sx={{ marginBottom: 2 }}>
      <CardContent>
        <Typography variant="h6" component="div">
          {task.title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {task.description}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Status: {task.status}
        </Typography>
        {task.status !== 'Done' && (
          <Button onClick={() => onMoveTask(task.id, 'In Progress')} variant="contained" sx={{ mr: 1, mt: 1 }}>
            Move to In Progress
          </Button>
        )}
        {task.status !== 'New' && (
          <Button onClick={() => onMoveTask(task.id, 'Done')} variant="contained" sx={{ mt: 1 }}>
            Move to Done
          </Button>
        )}
      </CardContent>
    </Card>
  );
};

export default Task;
