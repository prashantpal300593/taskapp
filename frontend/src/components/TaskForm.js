import React, { useState, useEffect } from 'react';
import { FormControl, InputLabel, Select, MenuItem, Button, TextField, Typography, CircularProgress, Grid } from '@mui/material';
import { createTask, getUsers, getAllProjects } from '../services/ApiCall'; // Import createTask, getUsers, and getAllProjects functions
import { useSelector } from 'react-redux';

const TaskForm = () => {
  const [assignedUserId, setAssignedUserId] = useState('');
  const [projectId, setProjectId] = useState('');
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [users, setUsers] = useState([]);
  const [projects, setProjects] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [createdAt, setCreatedAt] = useState('');

  useEffect(() => {
    const fetchUsersAndProjects = async () => {
      try {
        const userData = await getUsers();
        setUsers(userData);

        const projectData = await getAllProjects();
        setProjects(projectData);
      } catch (error) {
        console.error('Error fetching users and projects:', error);
      }
    };

    fetchUsersAndProjects();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!title || !description || !assignedUserId || !projectId) {
      setError('Please fill out all fields');
      return;
    }

    setLoading(true);
    setError('');

    const taskData = {
      title,
      description,
      assignedUserId,
      projectId,
    };

    try {
      const response = await createTask(taskData);
      // Optionally, you can reset the form fields here
      setTitle('');
      setDescription('');
      setAssignedUserId('');
      setProjectId('');
      setCreatedAt(response.createdAt); // Set the createdAt state with the value returned from backend
    } catch (error) {
      console.error('Error creating task:', error);
      setError('An error occurred while creating the task');
    } finally {
      setLoading(false);
    }
  };

  return (
    <Grid container justifyContent="center" alignItems="center" style={{ padding: '20px' }} className='container-from'>
      <Grid item xs={12} sm={8} md={6} lg={4} className='form-container'>
        <div>
          <Typography variant="h4" align="center" gutterBottom>
            Create New Task
          </Typography>
          <form onSubmit={handleSubmit}>
            <TextField
              label="Title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              fullWidth
              margin="normal"
              variant="outlined"
              color="primary"
            />
            <TextField
              label="Description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              fullWidth
              margin="normal"
              multiline
              rows={4}
              variant="outlined"
              color="primary"
            />
            <FormControl fullWidth margin="normal" variant="outlined">
              <InputLabel>User</InputLabel>
              <Select
                value={assignedUserId}
                onChange={(e) => setAssignedUserId(e.target.value)}
                label="User"
              >
                {users.map((user) => (
                  <MenuItem key={user._id} value={user._id}>
                    {user.username}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <FormControl fullWidth margin="normal" variant="outlined">
              <InputLabel>Project</InputLabel>
              <Select
                value={projectId}
                onChange={(e) => setProjectId(e.target.value)}
                label="Project"
              >
                {projects.map((project) => (
                  <MenuItem key={project._id} value={project._id}>
                    {project.name}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
       
            {error && (
              <Typography variant="body2" color="error" align="center" gutterBottom>
                {error}
              </Typography>
            )}
            <Button
              type="submit"
              variant="contained"
              color="primary"
              style={{ marginTop: '10px', width: '100%' }}
              disabled={loading}
            >
              {loading ? <CircularProgress size={24} /> : 'Create Task'}
            </Button>
          </form>
        </div>
      </Grid>
    </Grid>
  );
};

export default TaskForm;
