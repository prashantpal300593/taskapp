import { createTheme } from '@mui/material/styles';
import { Colors } from './Color';


const theme = createTheme({
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'capitalize',
          '&:hover': {
            backgroundColor: Colors.text,
            color: Colors.background, // Darker color on hover
          },
        },
        containedSuccess: {
          color: Colors.background,
          backgroundColor: Colors.success,

          '&:hover': {
            backgroundColor: Colors.success,
            color: Colors.background,// Darker color on hover
          },
        },
      },
    },

    MuiMenuItem: {
      styleOverrides: {
        root: {
          '& a': {
            color: "Colors.text", // Set the text color
            textDecoration: 'none', // Remove text decoration
            position: 'relative',
            '&::after': {
              content: '""',
              position: 'absolute',
              left: 0,
              right: 0,
              bottom: -2,
              height: 2,
              backgroundColor: 'transparent',
              transition: 'background-color 0.3s ease',
            },
            '&:hover::after': {
              backgroundColor: Colors.text, // Change the background color on hover
            },
          },
          textTransform: 'capitalize',
          color: Colors.text,
          textDecoration: 'none', // Apply text transform capitalize to all MenuItems
        },
      },
    },
    
  },
  typography: {
    fontFamily: [
      'CustomFont', // Replace with the name of your custom font
      'Arial', // Fallback font if CustomFont is not available
      'sans-serif',
    ].join(','),
    h1: {
      fontSize: '3rem',
      fontWeight: 500,
      color: Colors.text,
    },
    h2: {
      fontSize: '2.5rem',
      fontWeight: 600,
      color: Colors.text,
    },
    h3: {
      fontSize: '2rem',
      fontWeight: 500,
      color: Colors.text,
    },
    // Define typography settings for other variants as needed
  },
});

export default theme;
