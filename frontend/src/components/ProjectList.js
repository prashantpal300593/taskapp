import React, { useState, useEffect } from 'react';
import { getAllProjects, deleteProject, updateProjectName } from '../services/ApiCall';
import { List, Typography, Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField } from '@mui/material';
import Project from './Project';

const ProjectList = () => {
  const [projects, setProjects] = useState([]);
  const [loading, setLoading] = useState(true);
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
  const [editDialogOpen, setEditDialogOpen] = useState(false);
  const [projectToDelete, setProjectToDelete] = useState(null);
  const [projectToEdit, setProjectToEdit] = useState(null);
  const [editedProjectName, setEditedProjectName] = useState('');

  useEffect(() => {
    fetchProjects();
  }, []);

  const fetchProjects = async () => {
    try {
      const fetchedProjects = await getAllProjects();
      setProjects(fetchedProjects);
      setLoading(false);
    } catch (error) {
      console.error('Error fetching projects:', error);
      setLoading(false);
    }
  };

  const handleDeleteProject = async () => {
    try {
      await deleteProject(projectToDelete._id);
      setProjects(projects.filter(project => project._id !== projectToDelete._id));
      setDeleteDialogOpen(false);
    } catch (error) {
      console.error('Error deleting project:', error);
    }
  };

  const openDeleteDialog = (project) => {
    setProjectToDelete(project);
    setDeleteDialogOpen(true);
  };

  const closeDeleteDialog = () => {
    setProjectToDelete(null);
    setDeleteDialogOpen(false);
  };

  const openEditDialog = (project) => {
    setProjectToEdit(project);
    setEditedProjectName(project.name);
    setEditDialogOpen(true);
  };

  const closeEditDialog = () => {
    setProjectToEdit(null);
    setEditedProjectName('');
    setEditDialogOpen(false);
  };

const handleEditProject = async () => {
  try {
    // Make API call to update project details
    await updateProjectName(projectToEdit._id, editedProjectName);
    // Update projects state with the updated project
    setProjects(projects.map(project => project._id === projectToEdit._id ? { ...project, name: editedProjectName } : project));
    // Close the edit dialog
    closeEditDialog();
  } catch (error) {
    console.error('Error updating project:', error);
  }
};



  return (
    <div>
      {loading ? (
        <Typography>Loading...</Typography>
      ) : projects.length === 0 ? (
        <Typography>No projects available</Typography>
      ) : (
        <List>
          {projects.map((project) => (
            <Project key={project._id} project={project} onEdit={openEditDialog} onDelete={openDeleteDialog} />
          ))}
        </List>
      )}
      <Dialog open={deleteDialogOpen} onClose={closeDeleteDialog}>
        {/* Delete confirmation dialog content */}
      </Dialog>
      <Dialog open={editDialogOpen} onClose={closeEditDialog}>
        <DialogTitle>Edit Project</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            label="Project Name"
            value={editedProjectName}
            onChange={(e) => setEditedProjectName(e.target.value)}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={closeEditDialog} color="primary">
            Cancel
          </Button>
          <Button onClick={handleEditProject} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default ProjectList;
