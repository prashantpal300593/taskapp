import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import TaskColumn from './TaskColumn';
import { Grid, Typography, Tabs, Tab, Snackbar } from '@mui/material'; // Added Snackbar
import { getAllTasks, getTasksByUser, getProjectById, getUsers, updateTask } from '../services/ApiCall';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';

const ProjectDashboard = () => {
  const { projectId } = useParams();
  const [tasks, setTasks] = useState([]);
  const [projectName, setProjectName] = useState('');
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [selectedUserId, setSelectedUserId] = useState(null);
  const [selectedUserTasks, setSelectedUserTasks] = useState([]);
  const [users, setUsers] = useState([]);
  const [usernames, setUsernames] = useState({});
  const [stageChangeMessage, setStageChangeMessage] = useState(null); // Added stageChangeMessage state

  useEffect(() => {
    const fetchData = async () => {
      try {
        const projectData = await getProjectById(projectId);
        setProjectName(projectData.name);

        const tasksData = await getAllTasks(projectId);
        setTasks(tasksData);
        setSelectedUserTasks(tasksData); // Set selected user tasks initially

        const allUsers = await getUsers();
        setUsers(allUsers);

        const userNamesObject = {};

        allUsers.forEach(user => {
          userNamesObject[user.id] = user.username;
        });
        setUsernames(userNamesObject);

        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };

    fetchData();
  }, [projectId]);

  const handleTabChange = async (userId) => {
    try {
      if (userId !== null) {
        const userTasks = await getTasksByUser(projectId, userId);
        setSelectedUserId(userId);
        setSelectedUserTasks(userTasks);
      } else {
        setSelectedUserId(null);
        setSelectedUserTasks(tasks);
      }
    } catch (error) {
      console.error('Error fetching user tasks:', error);
    }
  };

  const onDragEnd = async (result) => {
    const { source, destination } = result;

    // Check if the drag ended outside of a droppable area
    if (!destination) {
      return;
    }

    // Check if the task was dropped in a different location
    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    // Optimistically update the UI
    const updatedTasks = [...selectedUserTasks];
    const draggedTaskIndex = updatedTasks.findIndex(task => task._id === result.draggableId);
    const draggedTask = updatedTasks[draggedTaskIndex];

    // Remove the task from its previous position
    updatedTasks.splice(draggedTaskIndex, 1);

    // Insert the task into its new position
    updatedTasks.splice(destination.index, 0, draggedTask);

    // Update the stage of the dragged task
    draggedTask.stage = destination.droppableId;

    try {
      // Update the task in the backend with the complete task object
      await updateTask(draggedTask._id, draggedTask);

      // Update the state with the new tasks
      setSelectedUserTasks(updatedTasks);

      // Show stage change message
      setStageChangeMessage(`Task moved to ${destination.droppableId} stage.`);
    } catch (error) {
      // If there's an error with the API call, revert the UI back to its previous state
      setSelectedUserTasks(selectedUserTasks);
      console.error('Error updating task:', error);
    }
  };

  const groupTasksByStage = (tasks) => {
    const groupedTasks = {
      'New': [],
      'In Progress': [],
      'Implemented': [],
      'Done': []
    };

    tasks.forEach(task => {
      groupedTasks[task.stage].push(task);
    });

    return groupedTasks;
  };

  const renderTaskColumns = () => {
    const groupedTasks = groupTasksByStage(selectedUserId ? selectedUserTasks : tasks);

    return Object.entries(groupedTasks).map(([stage, stageTasks]) => (
      <Grid item xs={3} key={stage}>
        <Droppable droppableId={stage}>
          {(provided) => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              <TaskColumn title={stage} tasks={stageTasks} />
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </Grid>
    ));
  };

  return (
    <div style={{ padding: '20px' }}>
      <Typography variant="h5" sx={{ marginBottom: '10px' }}>{projectName} Dashboard</Typography>
      {loading ? (
        <Typography>Loading...</Typography>
      ) : error ? (
        <Typography>Error: {error}</Typography>
      ) : (
        <>
          <Tabs value={selectedUserId} onChange={(event, newValue) => handleTabChange(newValue)}>
            <Tab label="All Tasks" value={null} />
            {users.map(user => (
              <Tab key={user._id} label={user.username} value={user._id} />
            ))}
          </Tabs>
          <DragDropContext onDragEnd={onDragEnd}>
            <Grid container spacing={3}>
              {renderTaskColumns()}
            </Grid>
          </DragDropContext>
       <Snackbar
          open={!!stageChangeMessage}
          autoHideDuration={6000}
          onClose={() => setStageChangeMessage(null)}
          message={stageChangeMessage}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} // Set anchor origin to top-center
        />

        </>
      )}
    </div>
  );
};

export default ProjectDashboard;
