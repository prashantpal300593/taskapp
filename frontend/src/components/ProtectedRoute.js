// ProtectedRoute.js
import React from 'react';
import { Route, Navigate } from 'react-router-dom';

const ProtectedRoute = ({ element: Element, ...rest }) => {
  // Check if user is authenticated (you can adjust the condition as per your authentication logic)
  const isAuthenticated = JSON.parse(localStorage.getItem('user')) !== null;

  return isAuthenticated ? (
    <Route {...rest} element={<Element />} />
  ) : (
    <Navigate to="/login" replace />
  );
};

export default ProtectedRoute;
