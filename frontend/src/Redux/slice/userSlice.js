import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  users: [],
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    addUser(state, action) {
      state.users = action.payload;
    },
    // Add other user-related reducers as needed
  },
});

export const { addUser } = userSlice.actions;

export default userSlice.reducer;
