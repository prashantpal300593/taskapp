import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  tasks: [],
};

const taskSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    // Reducer function to add a task
    addTask: (state, action) => {
      state.tasks.push(action.payload);
    },
    // Reducer function to remove a task
    removeTask: (state, action) => {
      state.tasks = state.tasks.filter(task => task._id !== action.payload);
    },
    // Reducer function to update a task
    updateTask: (state, action) => {
      const { id, updatedTask } = action.payload;
      const index = state.tasks.findIndex(task => task._id === id);
      if (index !== -1) {
        state.tasks[index] = { ...state.tasks[index], ...updatedTask };
      }
    },
    // Reducer function to mark a task as completed
    completeTask: (state, action) => {
      const { id, completed } = action.payload;
      const index = state.tasks.findIndex(task => task._id === id);
      if (index !== -1) {
        state.tasks[index].completed = completed;
      }
    },
    // Reducer function to set tasks
    setTasks: (state, action) => {
      state.tasks = action.payload;
    },
    // Reducer function to update task order
    updateTaskOrder: (state, action) => {
      state.tasks = action.payload;
    },
  },
});

export const { addTask, removeTask, updateTask, completeTask, setTasks, updateTaskOrder } = taskSlice.actions;
export default taskSlice.reducer;
