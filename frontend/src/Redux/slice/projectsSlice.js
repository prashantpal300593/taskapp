import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  projects: [],
};

const projectsSlice = createSlice({
  name: 'projects',
  initialState,
  reducers: {
    setProjects(state, action) {
      state.projects = action.payload;
    },
    addProject(state, action) {
      state.projects.push(action.payload);
    },
    removeProject(state, action) {
      state.projects = state.projects.filter(project => project.id !== action.payload);
    },
    updateProject(state, action) {
      const { id, updatedProject } = action.payload;
      const index = state.projects.findIndex(project => project.id === id);
      if (index !== -1) {
        state.projects[index] = { ...state.projects[index], ...updatedProject };
      }
    },
  },
});

export const { setProjects, addProject, removeProject, updateProject } = projectsSlice.actions;
export default projectsSlice.reducer;
