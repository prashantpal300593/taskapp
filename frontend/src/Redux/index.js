import { combineReducers } from '@reduxjs/toolkit';
import taskReducer from '../Redux/slice/taskSlice'; // Import your task reducer
import projectReducer from '../Redux/slice/projectsSlice'; // Import your project reducer
import userReducer from '../Redux/slice/userSlice'; // Import your user reducer

const rootReducer = combineReducers({
  tasks: taskReducer,
  projects: projectReducer,
  users: userReducer, // Include the user reducer
});

export default rootReducer;
