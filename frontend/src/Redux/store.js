// store.js

import { configureStore } from '@reduxjs/toolkit';
import rootReducer from '../Redux/index'; // Import the rootReducer from your reducers folder

const store = configureStore({
  reducer: rootReducer, // Pass the rootReducer to the store configuration
  // Add more configuration options as needed
});

export default store;
