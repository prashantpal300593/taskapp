import React, { useEffect, useState } from 'react';
import { Typography, Table, TableContainer, TableHead, TableBody, TableCell, TableRow, Paper, Button, Modal, Box, TextField, Grid } from '@mui/material';
import { getUsers, updateUser, deleteUser } from '../services/ApiCall'; // Import the service functions
import { useNavigate } from 'react-router-dom'; // Import useNavigate hook to navigate

const UserManagement = () => {
  const [users, setUsers] = useState([]);
  const [editUser, setEditUser] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const navigate = useNavigate(); // Initialize the useNavigate hook

  useEffect(() => {
    // Fetch all users when the component mounts
    fetchAllUsers();
  }, []);

  const fetchAllUsers = async () => {
    try {
      const response = await getUsers(); // Call the service function to fetch all users
      setUsers(response); // Update the state with the fetched users
    } catch (error) {
      console.error('Error fetching users:', error.message);
    }
  };

  const handleEditUser = (user) => {
    setEditUser(user);
    setIsModalOpen(true);
  };

  const handleDeleteUser = async (userId) => {
    try {
      // Call the service function to delete the user
      await deleteUser(userId);
      // Update the users state by filtering out the deleted user
      setUsers(users.filter(user => user._id !== userId));
    } catch (error) {
      console.error('Error deleting user:', error.message);
    }
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
    setEditUser(null);
  };

  const handleSaveUser = async () => {
    try {
      // Call the service function to update the user
      await updateUser(editUser._id, editUser);
      // Update the user in the users state
      setUsers(users.map(user => (user._id === editUser._id ? editUser : user)));
      handleCloseModal();
    } catch (error) {
      console.error('Error updating user:', error.message);
    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setEditUser({ ...editUser, [name]: value });
  };

  return (
    <div style={{padding:"10px"}}>
      <Typography variant="h4">Users</Typography>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Username</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Role</TableCell>
              <TableCell>Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((user) => (
              <TableRow key={user._id}>
                <TableCell>{user._id}</TableCell>
                <TableCell>{user.username}</TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>{user.role}</TableCell>
                <TableCell>
                  <Button variant="outlined" color="primary" onClick={() => handleEditUser(user)}>Edit</Button>
                  <Button variant="outlined" color="secondary" onClick={() => handleDeleteUser(user._id)}>Delete</Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      
      {/* Edit User Modal */}
      <Modal
        open={isModalOpen}
        onClose={handleCloseModal}
        aria-labelledby="modal-title"
        aria-describedby="modal-description"
      >
        <Box sx={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: 400, bgcolor: 'background.paper', boxShadow: 24, p: 4 }}>
          <Typography id="modal-title" variant="h6" component="h2" align="center" gutterBottom>
            Edit User
          </Typography>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                fullWidth
                name="username"
                label="Username"
                variant="outlined"
                value={editUser?.username || ''}
                onChange={handleInputChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                name="email"
                label="Email"
                variant="outlined"
                value={editUser?.email || ''}
                onChange={handleInputChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                fullWidth
                name="role"
                label="Role"
                variant="outlined"
                value={editUser?.role || ''}
                onChange={handleInputChange}
              />
            </Grid>
            <Grid item xs={12} sx={{ textAlign: 'right' }}>
              <Button variant="contained" color="primary" onClick={handleSaveUser}>Save</Button>
              <Button variant="contained" sx={{marginLeft:"10px"}} onClick={handleCloseModal}>Cancel</Button>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </div>
  );
};

export default UserManagement;
