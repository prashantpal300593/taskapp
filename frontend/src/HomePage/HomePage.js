import React from 'react';
import { Outlet, Link } from 'react-router-dom';
import { Container, Typography, Paper, Button } from '@mui/material';

const HomePage = () => {
  return (
    <div>
      {/* Banner section */}
      <Paper
        elevation={3}
        style={{
          marginBottom: '2rem',
          backgroundImage: `url('blank-papers-isolated-grey.jpg')`, // Update the URL to your image
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          textAlign: 'center',
          position: 'relative',
          width: '100%',
          margin: 0,
          borderRadius: 0,
          opacity: 0.8, // Apply opacity to the background image
          display:"flex",
          justifyContent:"center",
          alignItems:"center",
          height:"80vh"
        }}
      >
        {/* Text above banner */}
        <div style={{ marginBottom: '2rem' }}>
          <Typography variant="h3" gutterBottom style={{ color: '#000' }}> {/* Set text color to black */}
            Welcome to the Task Management App
          </Typography>
          <Typography variant="body1" paragraph style={{ color: '#000' }}> {/* Set text color to black */}
            This app helps you manage your tasks efficiently. You can add, edit, and delete tasks, as well as manage
            users and user permissions.
          </Typography>
          <Typography variant="body1" paragraph style={{ color: '#000' }}> {/* Set text color to black */}
            Get started by navigating to the Tasks page or User Management page using the links above.
          </Typography>
            <Link to="/tasks/add" style={{ textDecoration: 'none' }}>
          <Button variant="contained" color="primary">
            Create Task
          </Button>
        </Link>
        </div>
        {/* Create task button */}
      
      </Paper>

      {/* Render nested routes */}
      <Outlet />
    </div>
  );
};

export default HomePage;
