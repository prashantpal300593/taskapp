const mongoose = require('mongoose');

const projectSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  company: {
    type: String,
    required: false, // Make the company field optional
  },
  stages: {
    type: [String],
    default: ['New', 'In Progress', 'Implemented', 'Done'],
    validate: {
      validator: function (value) {
        // Ensure all elements in the array are strings
        return value.every(stage => typeof stage === 'string');
      },
      message: props => `${props.value} is not a valid stage name!`
    }
  },
  assignedUsers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }] // Array of user IDs assigned to this project
});

const Project = mongoose.model('Project', projectSchema);

module.exports = Project;
