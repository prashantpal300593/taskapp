require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// Import routes
const taskRoutes = require('../backend/routes/taskRoutes');
const authRoutes = require('../backend/routes/authRoutes');
const userRoutes = require('../backend/routes/userRoutes');
const projectRoutes = require('../backend/routes/projectRoutes');

const app = express();

// Increase payload size limit (e.g., 50MB)
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

// Connect to MongoDB
const username = "prashantCurdOperation";
const password = "12345";
const cluster = "cluster0.el895cd.mongodb.net";
const dbname = "curd";

mongoose.connect(
  `mongodb+srv://${username}:${password}@${cluster}/${dbname}`, 
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// Check for successful connection
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
  console.log("Connected successfully");
});

// Enable CORS
app.use(cors());

// Mount routes
app.use('/api/tasks', taskRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes); // Mount user routes at /api/users
app.use('/api/project', projectRoutes);

const PORT = process.env.PORT || 3002;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

