const bcrypt = require('bcrypt');
const User = require('../models/user');
const authService = require('../services/authService');

async function signup(req, res) {
  try {
    const { username, email, password, role, profilePic, phoneNumber, profession, companyName } = req.body;
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = new User({ username, email, password: hashedPassword, role, profilePic, phoneNumber, profession, companyName });
    await user.save();
    res.status(201).json({ message: 'User created successfully' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

async function login(req, res) {
  try {
    const { identifier, password } = req.body;
    const user = await User.findOne({
      $or: [{ username: identifier }, { email: identifier }],
    });

    if (!user) {
      return res.status(401).json({ message: 'Invalid username or password' });
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      return res.status(401).json({ message: 'Invalid username or password' });
    }

    // Generate token
    const token = authService.generateToken({ id: user._id, role: user.role });

    // Return user data along with token
    res.json({ token, user: { username: user.username, profilePic: user.profilePic, role: user.role } });
  } catch (error) {
    console.error('Error during login:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
}


module.exports = {
  signup,
  login
};
