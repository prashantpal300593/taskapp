const Project = require('../models/Project');

const createProject = async (req, res) => {
  try {
    const { name, company, assignedUsers } = req.body;
    const project = new Project({ name, company, assignedUsers }); // Include assignedUsers in the project data
    await project.save();
    res.status(201).json(project);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};


const getAllProjects = async (req, res) => {
  try {
    const projects = await Project.find();
    res.json(projects);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getProjectById = async (req, res) => {
  try {
    const project = await Project.findById(req.params.id);
    if (!project) {
      return res.status(404).json({ message: 'Project not found' });
    }
    res.json(project);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateProject = async (req, res) => {
  try {
    const projectId = req.params.id;
    const { name } = req.body;
    const updatedProject = await Project.findByIdAndUpdate(projectId, { name }, { new: true });
    if (!updatedProject) {
      return res.status(404).json({ error: 'Project not found' });
    }
    res.json(updatedProject);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};


 const updateProjectName = async (projectId, newName) => {
  try {
    const response = await callApi('PUT', `project/${projectId}`, { name: newName });
    console.log(response); // Log the response for debugging purposes
    return response; // Return the response if needed
  } catch (error) {
    // If an error occurs during the request, throw an error with a descriptive message
    throw new Error('Error updating project name: ' + error.message);
  }
};

const deleteProject = async (req, res) => {
  try {
    const projectId = req.params.id;
    // Use the deleteOne() method to delete the project by ID
    const deletedProject = await Project.deleteOne({ _id: projectId });
    if (deletedProject.deletedCount === 0) {
      // If no project was deleted, return a 404 status
      return res.status(404).json({ message: 'Project not found' });
    }
    // If the project was successfully deleted, return a success message
    res.json({ message: 'Project deleted successfully' });
  } catch (error) {
    // If an error occurs during the deletion process, return a 500 status with the error message
    res.status(500).json({ message: error.message });
  }
};



const assignUserToProject = async (req, res) => {
  try {
    const projectId = req.params.id;
    const { userId } = req.body;
    const project = await Project.findById(projectId);
    if (!project) {
      return res.status(404).json({ message: 'Project not found' });
    }
    // Assuming Project model has a field named 'assignedUsers' which is an array of user IDs
    project.assignedUsers.push(userId);
    await project.save();
    res.json({ message: 'User assigned to project successfully' });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createProject,
  getAllProjects,
  getProjectById,
  updateProject,
  deleteProject,
  updateProjectName,
  assignUserToProject
};
