// controllers/taskController.js
const Task = require('../models/task');
const User = require('../models/user');

/// Get all tasks
exports.getAllTasks = async (req, res) => {
  try {
    const tasks = await Task.find().select('title description stage projectId assignedUser createdAt');
    res.json(tasks);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
// Create a task

exports.createTask = async (req, res) => {
  try {
    const { title, description, assignedUserId, projectId } = req.body;

    // Check if the assigned user exists
    const assignedUser = await User.findById(assignedUserId);
    if (!assignedUser) {
      return res.status(400).json({ message: 'Assigned user not found' });
    }

    // Create the task with assigned user ID, project ID, and current timestamp
    const task = new Task({
      title,
      description,
      assignedUser: assignedUserId,
      projectId,
      createdAt: new Date() // Set createdAt field to current timestamp
    });

    // Save the task to the database
    await task.save();

    res.status(201).json(task);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};



// Get a task by ID
exports.getTaskById = async (req, res) => {
  try {
    const task = await Task.findById(req.params.id);
    if (!task) {
      return res.status(404).json({ message: 'Task not found' });
    }
    res.json(task);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Update a task by ID
// Update a task by ID
exports.updateTask = async (req, res) => {
  try {
    const { id } = req.params;
    const { title, description, stage, projectId } = req.body;

    // Validate required fields
    if (!title || !description || !stage || !projectId) {
      return res.status(400).json({ message: 'All fields (title, description, stage, projectId) are required' });
    }

    // Find the task by ID
    const task = await Task.findById(id);
    if (!task) {
      return res.status(404).json({ message: 'Task not found' });
    }

    // Update the task's fields
    task.title = title;
    task.description = description;
    task.stage = stage;
    task.projectId = projectId;

    // Save the updated task
    await task.save();

    res.json(task);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};







// exports.updateTask = async (req, res) => {
//   try {
//     const { id } = req.params;
//     const { stage } = req.body;

//     // Find the task by ID
//     const task = await Task.findById(id);
//     if (!task) {
//       return res.status(404).json({ message: 'Task not found' });
//     }

//     // Update the task's stage
//     task.stage = stage;

//     // Save the updated task
//     await task.save();

//     res.json(task);
//   } catch (error) {
//     res.status(500).json({ error: error.message });
//   }
// };

// Delete a task by ID
exports.deleteTask = async (req, res) => {
  try {
    const { id } = req.params;
    const task = await Task.findByIdAndDelete(id);
    if (!task) {
      return res.status(404).json({ message: 'Task not found' });
    }
    res.json({ message: 'Task deleted successfully' });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
exports.getTasksByUser = async (req, res)  => {
  try {
    const userId = req.params.userId;
    const tasks = await Task.find({ assignedUser: userId });
    res.json(tasks);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
}

// Update a task by ID when dropped
exports.updateTaskOnDrop = async (req, res) => {
  try {
    const { id } = req.params;
    const { stage } = req.body;

    // Find the task by ID
    const task = await Task.findById(id);
    if (!task) {
      return res.status(404).json({ message: 'Task not found' });
    }

    // Update the task's stage
    task.stage = stage;

    // Save the updated task
    await task.save();

    res.json(task);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};