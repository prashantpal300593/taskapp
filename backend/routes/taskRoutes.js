const express = require('express');
const router = express.Router();
const taskController = require('../controllers/taskController');

router.get('/', taskController.getAllTasks);
router.post('/', taskController.createTask);
router.get('/:id', taskController.getTaskById);
router.put('/:id', taskController.updateTask);
router.delete('/:id', taskController.deleteTask);
router.get('/:projectId/user/:userId', taskController.getTasksByUser);
router.put('/tasks/:id/drop', taskController.updateTaskOnDrop);



module.exports = router;
