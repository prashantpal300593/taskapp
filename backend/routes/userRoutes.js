const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');
const userController = require('../controllers/userController');

// Route to fetch all users
router.get('/', userController.getAllUsers);

// Route to fetch a single user by ID
router.get('/:userId', userController.getUserById);

// Route to delete a user by ID
router.delete('/:userId', userController.deleteUserById);

// Route to update user information by ID
router.put('/:userId', userController.updateUserById);

// POST route for user signup
router.post('/signup', authController.signup);

// POST route for user login
router.post('/login', authController.login);

module.exports = router;
